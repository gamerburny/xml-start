package at.gally.EZB;

import javax.xml.bind.annotation.XmlElement;

public class Kurs 
{
	 	@XmlElement(required = true)
	    protected String currency;
	    protected String rate;
	    
	    @Override
		public String toString() {
			return "Kurs [currency=" + currency + ", rate=" + rate+"]";
		}

		public String getCurrency() {
			return currency;
		}

		public void setCurrency(String currency) {
			this.currency = currency;
		}

		public String getRate() {
			return rate;
		}

		public void setRate(String rate) {
			this.rate = rate;
		}
	    
}
